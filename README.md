# Summary

This project is designed to check the IP addresses of devices connected in a network. A single IP address will be selected and each IP address in its possible range will be pinged. It outputs all devices connected in a network, the selected device's IP address and all IP addresses that responded on the network.


# How to use it

Run the Shell Script called run.sh located within the project's folder. The program is using ipconfig and ping and may run only on Windows OS unless specifically configured.


# What this template project contains

README.md
Run.sh


## Source code 

Run.sh is located under `source` directory. This directory contains all the necessary scripts that implement the project.